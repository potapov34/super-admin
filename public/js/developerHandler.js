$(document).ready(function(e) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function printErrorMsg (msg) {
        $(".validation").find("ul").html('');
        $(".validation").css('display','block');
        $.each( msg, function( key, value ) {
            $(".validation").find("ul").append('<li>'+value+'</li>');
        });
    }

    $(".update-developer").click(function(e) {
        e.preventDefault();

        var idDeveloper = $(this).attr('data-id');
        var developerName = $('.developer-name').val();
        var monitorsName = $(".dev-monitors").val();


        $.ajax({
            type: "PATCH",
            url: "/developers/"+ idDeveloper,
            data: { name:developerName, monitors_name: monitorsName},
            success: function(data) {
                if($.isEmptyObject(data.error)) $(location).attr('href',"/developers");
                else printErrorMsg(data.error);
            }
        });
    });

    $('.add-developer').click(function(e) {
        e.preventDefault();

        var developerName = $(".developer-name").val();
        var monitorsName = $(".dev-monitors").val();

        $.ajax({
            type: "POST",
            url: "/developers",
            data: { name: developerName, monitors_name: monitorsName },
            success: function(data) {
                if($.isEmptyObject(data.error)) $(location).attr('href',"/developers");
                else printErrorMsg(data.error);
            }
        });
    });

    $('.delete-developer').click(function(e) {
        e.preventDefault();

        var developerId = $(this).attr('data-id');
        var nameDeveloper = $(this).attr('data-name');


        $("#modal-developer-name").text(nameDeveloper);

        $('.confirm-delete').click(function(e) {
            $.ajax({
                type: "DELETE",
                url: "/developers/" + developerId,
                data: {},
                success: function() {
                    $(location).attr('href',"/developers");
                }
            });
        });
    });
});