$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function printErrorMsg (msg) {
        $(".validation").find("ul").html('');
        $(".validation").css('display','block');
        $.each( msg, function( key, value ) {
            $(".validation").find("ul").append('<li>'+value+'</li>');
        });
    }


    $('.update-monitor').click(function(e) {
        e.preventDefault();

        var idMonitor = $('.update-monitor').attr('data-id');
        var brand = $('.brand-name').val();
        var model = $('.model-name').val();
        var idDeveloper = $('.developer-name').val();



        $.ajax({
            type: "PATCH",
            url: "/monitors/"+ idMonitor,
            data: { brand_name: brand, model_name: model, developer_name: idDeveloper },
            success: function(data) {
                if($.isEmptyObject(data.error)) $(location).attr('href',"/monitors");
                else printErrorMsg(data.error);
            }
        });
    });

    $('.add-monitor').click(function(e) {
        e.preventDefault();

        var brandName = $('.brand-name').val();
        var modelName = $('.model-name').val();
        var idDeveloper = $('.developer-name').val();

        $.ajax({
            type: "POST",
            url: "/monitors",
            data: { brand: brandName, model: modelName, developer_name: idDeveloper },
            success: function(data) {
                if($.isEmptyObject(data.error)) $(location).attr('href',"/monitors");
                else printErrorMsg(data.error);
            }
        });
    });

    $('.delete-monitor').click(function(e) {
        e.preventDefault();

        var monitorId = $(this).attr('data-id');
        var nameMonitor = $(this).attr('data-name');

        $("#modal-monitor-name").text(nameMonitor);

        $('.confirm-delete').click(function(e) {
            $.ajax({
                type: "DELETE",
                url: "/monitors/" + monitorId,
                data: {},
                success: function() {
                    $(location).attr('href',"/monitors");
                }
            });
        });
    });
});