<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <title>Мониторы</title>
</head>
<body>
<div class="wrapper">
    <div class="wrap-link">
        <a class="btn btn-primary addCountry" href="/monitors/create">Добавить Монитор</a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Монитор</th>
            <th scope="col">Разработчики</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paginate as $monitor)
            <tr>
                <td>
                    {{$monitor->brand}}
                    {{$monitor->model}}
                </td>
                <td>
                    {{$monitor->developers->implode('name', ', ')}}
                </td>

                <td><a class="btn btn-success" href="/monitors/{{$monitor->id}}/edit">Редактировать</a></td>
                <td><button class="btn btn-danger delete-monitor" data-id="{{$monitor->id}}" data-name="{{$monitor->brand}} {{$monitor->model}}" data-toggle="modal" data-target="#exampleModalCenter">Удалить</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$paginate}}
    <a href="/">Назад</a>
</div>
<video muted autoplay loop>
    <source src="{{URL::asset('css/Words.mp4')}}" type="video/mp4">
</video>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Подтверждение удаления</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    Вы уверены что хотите удалить <span id="modal-monitor-name"></span>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                    <button type="button"  class="btn btn-primary confirm-delete">Да</button>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="{{URL::asset('js/monitorHandle.js')}}"></script>
</body>
</html>

