<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <title>Добавить монитор</title>
</head>
<body>
<div class="wrapper">
    <form>
        {{ csrf_field() }}
        <div class="input-group mb-3 input">
            <div class="form-group">
                <label for="">Название бренда</label>
                <input type="text" class="form-control brand-name" placeholder="Название бренда" value="" name="brand_name" aria-describedby="basic-addon1">
            </div>
            <div class="form-group">
                <label for="">Название модели</label>
                <input type="text" class="form-control model-name" placeholder="Название модели" value="" name="model_name" aria-describedby="basic-addon1">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect2">Разработчики</label>
                <select multiple name="developer_name" class="form-control developer-name" id="exampleFormControlSelect2">
                    @foreach($developers as $developer)
                        <option value="{{$developer->id}}">{{$developer->name}}</option>
                    @endforeach
                </select>
            </div>
            <input type="submit" class="btn btn-success add-monitor" name="add" value="Добавить">
        </div>
    </form>
    <div class="validation">
        <ul></ul>
    </div>
    <a href="/monitors">Назад</a>
</div>
<video muted autoplay loop>
    <source src="{{URL::asset('css/Travaho.mp4')}}" type="video/mp4">
</video>
<script src="{{URL::asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{URL::asset('js/monitorHandle.js')}}"></script>
</body>
</html>