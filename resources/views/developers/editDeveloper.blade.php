<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <title>Редактировать разработчика</title>
</head>
<body>
<div class="wrapper">
    <form>
        {{ csrf_field() }}
        <div class="input-group mb-3 input">
            <div class="form-group">
                <label>Имя разработчика</label>
                <input type="text" class="form-control developer-name" maxlength="30" value="{{$developer->name}}" placeholder="Имя разработчика" name="developer_name" aria-label="Имя разработчика" aria-describedby="basic-addon1">
            </div>
            <div class="form-group">
                <label>Мониторы</label>
                <select multiple name="" class="form-control dev-monitors" id="dev-monitors">

                    @foreach($monitors as $monitor)
                        <option @if($developer->getSelectedDevelopersHtml()->contains($monitor->id)){{'selected'}} @endif value="{{$monitor->id}}">{{$monitor->brand}} {{$monitor->model}}</option>
                    @endforeach
                </select>
            </div>
            <input type="submit" class="btn btn-success update-developer" data-id="{{$developer->id}}" name="add" value="Обновить">
        </div>
    </form>
    <div class="validation">
        <ul></ul>
    </div>
    <a href="/developers">Назад</a>
</div>
<video muted autoplay loop>
    <source src="{{URL::asset('css/Travaho.mp4')}}" type="video/mp4">
</video>
<script src="{{URL::asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{URL::asset('js/developerHandler.js')}}"></script>
</body>
</html>