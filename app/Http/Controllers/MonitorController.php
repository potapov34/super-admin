<?php

namespace App\Http\Controllers;

use App\Developer;
use Illuminate\Http\Request;
use App\Monitor;
use Validator;

class MonitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginate = Monitor::with('developers')->paginate(10);

        return view('monitors/index', ['paginate' => $paginate]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $developers = Developer::all();

        return view('monitors/addMonitor', ['developers' => $developers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand' => 'required|alpha_num|max:20',
            'model' => 'required|alpha_num|max:20',
        ]);

        if($validator->passes()) {
            $developers = $request->input('developer_name');


            $monitor = Monitor::create($request->all());

            Monitor::findOrFail($monitor->id)->developers()->sync($developers);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $developers = Developer::all();
        $monitor = Monitor::findOrFail($id);
        return view('/monitors/editMonitor', ['monitor' => $monitor, 'developers' => $developers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required|alpha_num|max:20',
            'model_name' => 'required|alpha_num|max:20',
        ]);

        if($validator->passes()) {
            $developers = $request->input('developer_name');

            $monitor = Monitor::findOrFail($id);
            $monitor->brand = $request->input('brand_name');
            $monitor->model = $request->input('model_name');
            $monitor->save();

            Monitor::findOrFail($id)->developers()->sync($developers);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Monitor::destroy($id);
    }
}
