<?php

namespace App\Http\Controllers;

use App\Monitor;
use Illuminate\Http\Request;
use App\Developer;
use Validator;

class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginate = Developer::with('monitors')->paginate(10);

        return view('developers/index', ['paginate' => $paginate]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $monitors = Monitor::all();

        return view('developers/addDeveloper', ['monitors' => $monitors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha|max:30'
        ]);

        if($validator->passes()) {
            $monitors = $request->input('monitors_name');

            $developer = Developer::create($request->all());

            Developer::findOrFail($developer->id)->monitors()->sync($monitors);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $monitors = Monitor::all();
        $developer = Developer::findOrFail($id);

        return view('developers/editDeveloper', ['developer' => $developer, 'monitors' => $monitors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha|max:30'
        ]);

        if($validator->passes()) {

            $monitors = $request->input('monitors_name');

            $developer = Developer::findOrFail($id);
            $developer->name = $request->input('name');
            $developer->save();

            Developer::findOrFail($id)->monitors()->sync($monitors);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Developer::destroy($id);
    }
}
