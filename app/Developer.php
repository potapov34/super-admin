<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public function monitors() {
        return $this->belongsToMany('App\Monitor');
    }

    public function getMonitorsHtml()
    {
        $monitors = collect();

        foreach ($this->monitors as $item) {
            $monitors->push($item->brand . ' ' . $item->model);
        }

        return $monitors->implode(', ');
    }

    public function getSelectedDevelopersHtml() {
        $selected_developer = collect();

        foreach ($this->monitors as $item) {
            $selected_developer->push($item->id);
        }

        return $selected_developer;
    }


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
    }
}
