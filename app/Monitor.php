<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitor extends Model
{

    public $timestamps = false;
    protected $fillable = ['brand','model'];

    public function developers() {
        return $this->belongsToMany('App\Developer');
    }

    public function setBrandAttribute($value)
    {
        $this->attributes['brand'] = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
    }

    public function getSelectedMonitorsHtml() {
        $selected_monitors = collect();

        foreach ($this->developers as $item) {
            $selected_monitors->push($item->id);
        }

        return $selected_monitors;
    }
}
