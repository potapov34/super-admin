## Super Admin
Мой проект Super Admin

### Как запустить?

1. Склонировать проект
`git clone https://potapov34@bitbucket.org/potapov34/super-admin.git`

2. Перейти в директорию super-admin и клонировать подмодули командами:
`git submodule init` и `git submodule update`

3. Скопировать и переименовать файл `.evn.example` в `.evn`

4. Перейти в директорию super-admin/laradock-super-admin

5. Скопировать и переименовать файл из `evn.example` в `.evn`

6. Выполнить команды `./up.sh` и `./workspace.sh`

7. Выполнить команду `composer install` для загрузки зависимостей

8. Выполнить команду `artisan key:generate`

9. Выполнить миграцию и сид

Перейти на сайт `super-admin.localhost`

Всё работает!


### Как выполнить миграцию/сид

 Перейти в директорию super-admin/laradock-super-admin  и запустить команды `./workspace.sh`,
  `art migrate` для миграции и `art db:seed` для сида.
