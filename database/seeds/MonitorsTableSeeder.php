<?php

use Illuminate\Database\Seeder;

class MonitorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('monitors')->insert(
            [
                [
                    'brand' => 'LG',
                    'model' => '2322'
                ],
                [
                    'brand' => 'Samsung',
                    'model' => 'em918q'
                ],
                [
                    'brand' => 'acer',
                    'model' => '330mpw1'
                ],
            ]
        );
    }
}
